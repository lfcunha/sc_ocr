__author__ = 'luiscunha'


import pytesseract
from PIL import Image
from PIL import ImageFilter
from StringIO import StringIO
import MySQLdb
import os
import time
from time import gmtime, strftime
import codecs
import unicodedata

HOST = os.environ.get("HOST")
USER = os.environ.get("USER")
PASSWD =os.environ.get("PASSWD")
DB = os.environ.get("DB")


def process_image(image_file_data):
    """Get image raw data from Redis. Do text extraction with pytesseract and save to MySQL
    :param image_file_data:
    :return img_text:
    """
    try:
        conn = MySQLdb.connect(host=HOST, user=USER, passwd = PASSWD, db = DB)
        cur = conn.cursor()
        cur.execute('SET NAMES utf8;')
        cur.execute('SET CHARACTER SET utf8;')
        cur.execute('SET character_set_connection=utf8;')
    except MySQLdb.Error, e:
        # To do: Handle database connection failure
        print e

    image = _get_image(image_file_data["data"])
    image.filter(ImageFilter.SHARPEN)
    # Resize Image
    basewidth = 1500
    wpercent = (basewidth / float(image.size[0]))
    hsize = int((float(image.size[1]) * float(wpercent)))
    image = image.resize((basewidth, hsize), Image.ANTIALIAS)
    #image.save('resized_image.jpg')
    img_text  = pytesseract.image_to_string(image)
    img_text = unicode(img_text.strip(codecs.BOM_UTF8), 'utf-8')
    img_text = unicodedata.normalize('NFKD', img_text).encode('ascii','ignore')
    ts = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    name = image_file_data["fname"]
    query = "INSERT INTO `ocr`.`ocr` (`id`, `name`, `image`, `text`, `user`, `created_at`, `modified_at`) VALUES (NULL, %s, %s, %s, %s, %s, CURRENT_TIMESTAMP)"
    # use default user 'lfcunha'. In the next version, add user login capabilities
    cur.execute(query, (name, "NULL", img_text, 'lfcunha', ts))
    conn.commit()
    conn.close()
    return img_text


def _get_image(image_file_data):
    buff = StringIO()
    buff.write(image_file_data)
    buff.seek(0)
    return Image.open(buff)
