__author__ = 'luiscunha'

import os
from redis import Redis
from rq import Worker, Queue, Connection


listen = ['low']

redis_url = os.getenv('REDIS_URL')
if not redis_url:
    raise RuntimeError('Set up Redis first.')

conn = Redis(host=redis_url)

with Connection(conn):
    worker = Worker(map(Queue, listen))
    worker.work()