__author__ = 'luiscunha'

import os
import random
import string

import tornado.web

from rq import Queue
from redis import Redis
from ocr_microservice.ocr import process_image


redis_url = os.getenv("REDIS_URL")
loadbalancer_url = os.getenv("LOADBALANCER_URL")


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This microservice only accepts POST requests")


class UploadHandler(tornado.web.RequestHandler):
    def post(self):
        file1 = self.request.files['file1'][0]
        original_fname = file1['filename']
        extension = os.path.splitext(original_fname)[1]
        fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))
        final_filename= fname+extension

        redis_conn = Redis(host=redis_url)
        q = Queue("low",connection=redis_conn)
        job = q.enqueue(process_image, {"data": file1['body'], "fname":fname})
        self.redirect(loadbalancer_url)


application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/upload", UploadHandler)
])


if __name__ == "__main__":
    application.listen(8889)
    tornado.ioloop.IOLoop.instance().start()