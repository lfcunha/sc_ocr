# README #

# #
## Demo of OCR Application##

# #
To install the required packages, create a virtual environment and run:
    
```
#!bash

$ pip install -r requirements.txt
```

Set up your Redis and MySQL servers and add these environmental variables to your bash profile:
```
#!bash
export HOST=<MySQL_HOST>
export USER=<MySQL_USER>
export PASSWD=<MySQL_PASS>
export DB="ocr"
export REDIS_URL=<REDIS_IP>
export LOADBALANCER_URL=<http://loadbalancer_url>
```

And Finally start all three applications: the website, the file upload/queuing micro-service and the OCR workers on each machine:

```
#!Bash

$ python run.py
$ python ocr_microservice/tornado_upload_image_microservice.py
$ python ocr_microservice/run_worker.py
```


### Application Structure ###

* The website (a flask app) consists of two views: 
 1. A View of scanned documents (stored in MySQL); 
 2. A form to upload new image to OCR
* a Tornado microservice receives the image and queues it into redis
* Workers perform OCR and store the result in MySQL
* Task queueing uses the rq module (a resque implemention for python) - A production application would make use of Celery/RabbitMQ
* On a production app, ngnix would be placed behing HAPROXY to serve static content
* 
*To programmatically access the api, POST the image to http://104.131.71.10:8889 or http://45.55.236.72:8889 (e.g. with curl)

### HARDWARE ###
* The application runs on a 5 node virtual private cloud on digital ocean
* It consists of:
*     1 loadbalancer node; 
      2 VM running the website; 
      2 VM each running a Tornado webserver OCR microservice and 2 workers performing the OCR (with tesseract)
* 
* Tornado was chosen because it's a non-blocking server. The microservice reads the uploaded file and queues it to Redis for OCR processing
* 
* Application replicas are bound to the public ip, but database (redis and mysql) and internal communication is bound to the private network IPs
*
* Node1: (http://45.55.236.68):
      - 1 CPU: runs HAPROXY LoadBalancer and the Redis database (for task queuing)
* Node2 and Node3  (http://45.55.236.49:8081 and http://45.55.236.77:8081)  : 
      - 1 CPU each: runs Flask webapp with 2 views:  1) view documents in the Mysql Database   2) webpage to upload 
                  ; also runs the MySQL server (node3)
* Node4 and Node5 (http://104.131.71.10:8889/  and http://45.55.236.72:8889):
      - 2 CPU each: each run a Tornado webserver microservice which reads the uploaded image and queues it for OCR
                    - and each run two worker processes (4 total) which perform the OCR and upload the text to MySQL
*                    
*                    
*
* Try it with a single document upload on the webpage, or put it through a stress test:
* POST to http://104.131.71.10:8889  - this is a direct access to one of the tornado servers, with 2 workers, so not load balanced


![ocr_vpc.png](https://bitbucket.org/repo/oKg6kG/images/1428906184-ocr_vpc.png)