# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

from app import app
from app.ocr.models import Ocrs
from app.ocr.models import Ocr


mod_ocrs = Blueprint('ocrs', __name__, url_prefix='/')
mod_ocr = Blueprint('ocr', __name__, url_prefix='/ocr/')


@mod_ocrs.route('/', methods=['GET'])
def ocrs():
    """Render page to display current ocr in the database
    :return index.html:
    """
    with open("log.txt", "a") as logger:
        logger.write(request.remote_addr)
    _ocrs = Ocrs()
    return render_template('ocr/index.html', ocrs_list=_ocrs.ocr_list)


@mod_ocr.route('', methods=['GET'])
def ocr():
    """Render page to display current ocr in the database
    :return index.html:
    """
    _movies = Ocrs()

    if request.method == 'GET':
        return render_template('ocr/new_ocr.html')


# Register missing page handler
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


