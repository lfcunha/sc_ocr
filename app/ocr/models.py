# coding=utf-8
__author__ = 'luiscunha'

import MySQLdb
from datetime import date
import sys
from app import app
import codecs
import unicodedata

class Ocrs:
    """Class Ocrs represents a collection of ocr objects
    """

    def __init__(self):
        """
        :param database:
        :return:
        """
        try:
            self._conn = MySQLdb.connect(host=app.host, user=app.user, passwd=app.passwd, db=app.db)
            self._cursor = self._conn.cursor()
        except MySQLdb.Error, e:
            # To do: log error and display a graceful error to the user
            print e
        self._ocr_list = []

    def select(self):
        """Select all ocr from database and return a list of movie objects
        :return _movies_list:
        """
        self._cursor.execute("select * from ocr order by id desc")
        ocr_list = self._cursor.fetchall()
        self._conn.close()
        for ocr_ in ocr_list:
            ocr = Ocr()
            text = unicode(ocr_[3].strip(codecs.BOM_UTF8), 'utf-8')
            text = unicodedata.normalize('NFKD', text).encode('ascii','ignore')
            ocr.id, ocr.name, ocr.image, ocr.text, ocr.user, ocr.created_at, ocr.modified_at = \
                ocr_[0], ocr_[1], ocr_[2], text, ocr_[4], ocr_[5], ocr_[6]
            self._ocr_list.append(ocr)
        return self._ocr_list


    @property
    def ocr_list(self):
        return self.select()


class Ocr:
    """An OCR model
    """

    def __init__(self):
        """Inject database to allow for tests with custom database
        :param database:
        :return:
        """
        self._id = None
        self._name = None
        self._text = None
        self._user = None
        self._created_at = None

    def insert(self):
        pass

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._title = id

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        self._created_at = created_at


if __name__ == "__main__":
        m = Ocrs()
        for x in m.ocr_list:
            print x.text
