# coding=utf-8
__author__ = 'luiscunha'

from flask import Flask
import os
app = Flask(__name__)


# Configurations #

app.db = os.environ['DB']
app.host = os.environ['HOST']
app.user = os.environ['USER']
app.passwd = os.environ['PASSWD']
app.redis_url = os.getenv("REDIS_URL")
app.loadbalancer_url = os.getenv("LOADBALANCER_URL")


from app.ocr.controller import mod_ocrs as ocrs_module
from app.ocr.controller import mod_ocr as ocr_module


app.register_blueprint(ocrs_module)
app.register_blueprint(ocr_module)



if __name__ == "__main__":
    pass









