Flask==0.10.1
Jinja2==2.7.3
MySQL-python==1.2.5
nose==1.3.4
requests==2.6.0
tornado==4.1
Werkzeug==0.10.4
backports.ssl-match-hostname==3.4.0.2
certifi==2015.4.28
click==4.0
itsdangerous==0.24
MarkupSafe==0.23
PIL==1.1.7
pytesseract==0.1.6
redis==2.10.3
requests==2.7.0
rq==0.5.2